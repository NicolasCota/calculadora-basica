/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;
import java.util.Scanner;
/**
 *
 * @author usuario
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int opc;
        Funcionesalgebraicas FAL = new Funcionesalgebraicas();
        Funcionestrigonometricas FT = new Funcionestrigonometricas();
        Funcionesaritmeticas FAR =new Funcionesaritmeticas();

        System.out.println("Bienvenidos a calculadora");
        System.out.println ("introduzca el digito para correspondiente a la operación que desea realizar");
        System.out.println("1 para suma");
        System.out.println("2 para resta");
        System.out.println("3 para multiplicación");
        System.out.println("4 para división");
        System.out.println("5 para exponencial");
        System.out.println("6 para logaritmos");
        System.out.println("7 para potenciación");
        System.out.println("8 para seno");
        System.out.println("9 para coseno");
        System.out.println("10 para tangente");
        System.out.println("11 para secante");
        System.out.println("12 para cosecante");
        System.out.println("13 para cotangente");
        
        opc = entrada.nextInt ();
        switch (opc) {
            
            case 1: FAL.Suma();
                  break;
            case 2: FAL.Resta();
                  break;
            case 3: FAL.Multiplicacion();
                  break;
            case 4: FAL.Division();
                  break;
            case 5: FAR.Exponencial();
                  break;
            case 6: FAR.Logaritmo();
                  break;
            case 7: FAR.Potenciacion();
                  break;
            case 8: FT.Seno();
                  break;
            case 9: FT.Coseno();
                  break;
            case 10: FT.Tangente();
                break;
            case 11: FT.Secante();
                break;
            case 12: FT.Cosecante();
                break;
            case 13: FT.Cotangente();
                break;
        }
        
    }
    
}
