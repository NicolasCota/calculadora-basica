/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.util.Scanner;

/**
 *
 * @author usuario
 */
public class Funcionesalgebraicas {

    double numero1 = 0, numero2 = 0;
    Scanner entrada = new Scanner(System.in);

    void Suma() {
        System.out.println("a+b=c");
        System.out.print("Digite a=: ");
        numero1 = entrada.nextDouble();
        System.out.print("Digite b=: ");
        numero2 = entrada.nextDouble();
        System.out.println("c= " + (numero1 + numero2));
    }

    void Resta() {
        System.out.println("a-b=c");
        System.out.print("Digite a=: ");
        numero1 = entrada.nextDouble();
        System.out.print("Digite b=: ");
        numero2 = entrada.nextDouble();
        System.out.println("c= " + (numero1 - numero2));
    }

    void Multiplicacion() {
        System.out.println("a*b=c");
        System.out.print("Digite a=: ");
        numero1 = entrada.nextDouble();
        System.out.print("Digite b=: ");
        numero2 = entrada.nextDouble();
        System.out.println("c= " + (numero1 * numero2));
    }

    void Division() {
        System.out.println("a/b=c");
        System.out.print("Digite a=: ");
        numero1 = entrada.nextDouble();
        do {
            System.out.print("Digite b=: ");
            numero2 = entrada.nextDouble();
        } while (numero2 == 0);
        System.out.println("c= " + (numero1 / numero2));

    }
}
