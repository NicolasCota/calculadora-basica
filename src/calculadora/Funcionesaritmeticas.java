/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;
import java.util.Scanner;
/**
 *
 * @author usuario
 */
public class Funcionesaritmeticas {
Scanner entrada = new Scanner(System.in);
double numero1 = 0, numero2 = 0;
    void Exponencial() {
        System.out.println("a*e^b=c");
        System.out.print("Digite a=: ");
        numero1 = entrada.nextDouble();
        System.out.print("Digite b=: ");
        numero2 = entrada.nextDouble();
        System.out.println("c= " + (numero1 * Math.pow(Math.E, numero2)));
    }

    void Logaritmo() {
        System.out.println("Logaritmo en base a de b=c");
        System.out.print("Digite a=: ");
        numero1 = entrada.nextDouble();
        System.out.print("Digite b=: ");
        numero2 = entrada.nextDouble();
        System.out.println("c= " + (Math.log(numero2)/Math.log(numero1)));
    }

    void Potenciacion() {
        System.out.println("a^b=c");
        System.out.print("Digite a=: ");
        numero1 = entrada.nextDouble();
        System.out.print("Digite b=: ");
        numero2 = entrada.nextDouble();
        System.out.println("c= " +Math.pow(numero1, numero2) );
    }
}
